setTimeout(function () {
    $('#failMessage').fadeOut('fast');
}, 8000);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var url = null;

$.ajax({
    url: url,
    method: 'POST',
    data: $('#prod_form').serialize(),
    error: function (error) {
        console.log(error)
    }
});

var table = null;
var ajax = null;

function renderDtTable() {

    table = $('#productTable').DataTable({
        ajax: ajax,
        "dataSrc": "data",
        'processing': true,
        // 'serverSide': true,
        'pageLength': 3,
        'searching': true,
        'stateSave': false,
        'columns': [{
            data: 'ID'
        },
        {
            data: 'Name'
        },
        {
            data: 'Category Name'
        },
        {
            data: 'Product Code'
        },
        {
            data: 'Price/Sale Price'
        },
        {
            data: 'Order'
        },
        {
            data: 'Quantity'
        },
        {
            data: 'Added Date'
        },
        {
            data: 'Modified Date'
        },
        {
            data: 'Status'
        },
        {
            data: 'Image',
            orderable: false
        },
        {
            data: 'action',
            orderable: false
        },
        ],

    });
}



$(document).ready(function (e) {

    $('#productTable thead tr').clone(true).appendTo('#productTable thead');
    $('#productTable thead tr:eq(0) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" style="width: 100%;" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });

        $('#reset_btn_id').on('click', function () {
            table
                .search('')
                .columns().search('')
                .draw();
            $("input[type=text]").val("");
        });
    });

    renderDtTable();
    $("#e1").select2();
    $("#statusid").select2();

});