//data insert
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var url = null;

$.ajax({
    url: url,
    method: 'POST',
    data: $('#cat_form').serialize(),
    error: function (error) {
        console.log(error)
    }
});


//datatable
var table = null;
var ajax = null;

function renderDtTable() {

    table = $('#categoryTable').DataTable({
        ajax: ajax,
        "dataSrc": "data",
        'processing': true,
        // 'serverSide': true,
        'pageLength': 3,
        'searching': true,
        'stateSave': false,
        'columns': [{
            data: 'ID'
        },
        {
            data: 'Name'
        },
        {
            data: 'Order'
        },
        {
            data: 'Added Date'
        },
        {
            data: 'Modified Date'
        },
        {
            data: 'Status'
        },
        {
            data: 'No of Product'
        },
        {
            data: 'Image',
            orderable: false
        },
        {
            data: 'action',
            orderable: false
        },
        ],

    });
}



$(document).ready(function (e) {

    $('#categoryTable thead tr').clone(true).appendTo('#categoryTable thead');
    $('#categoryTable thead tr:eq(0) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" style="width: 100%;" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });

        $('#reset_btn_id').on('click', function () {
            table
                .search('')
                .columns().search('')
                .draw();
            $("input[type=text]").val("");
        });
    });

    renderDtTable();
    $("#statusid").select2();

});