@extends('masterlayout')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>project</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
 
  <style>
#thumbnil{
  height:150px;
  width: 90px;
}
</style>

</head>
<body>
<div>

<div class="container">

  <h2>edit category</h2>

  <br>
<form class="form-horizontal" id="categoryForm" action="update/{{$edit->id}}"  method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT');
  {{-- <div class="form-group">
      <label class="control-label col-sm-2" for="name">name</label>
      <div class="col-sm-10"> --}}
    <input type="hidden" class="form-control"  id="id"  name="id"  placeholder="Enter name" value="{{ $edit->id }}" >
    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="status">name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  id="name"  name="name"  placeholder="Enter name"   value="{{ $edit->name }}" required="">
    </div>
   <br>
    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="status">order</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" id="order"  name="order"  value="{{ $edit->order }}" required="">
      </div>

    <br>
    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="status">status</label>
      <div class="col-sm-10">
        <select class="form-control" name="status" required="">

          {{-- <option>select status</option>
          <option value="active" {{ ($edit->status ='active') 
         "selected";
    
        }}>active</option>
          <option value="inactive"  {{ ($edit->status ='active') 
             "selected";
        
            }}>inactive</option>
 --}}

           <option value="">select status</option>
          <option value="active" {{isset(request()->id) ? ($edit->status='active') ? "selected" :'':''}}>active</option>
          <option value="inactive" {{isset(request()->id) ? ($edit->status='inactive') ? "selected" :'':''}}>inactive</option>
        </select>
      </div>
    </div>
    <br>

    <div class="form-group">
        <div class="custom-file">
            <label class="control-label col-sm-2" for="image">image:</label>
            <div class="col-sm-10">
            <input type="file" accept="image/*" class="custom-file-left-input" name="image" onchange="showMyImage(this)" id="fileupload" lang="es" required="">
            <br>
            <img id="thumbnil" style=" margin-top:10px;" src="" alt="image"/>
            <script>
      function showMyImage(fileInput) {
      var files = fileInput.files;
     for (var i = 0; i < files.length; i++) {
     var file = files[i];
     var imageType = /image.*/;
     if (!file.type.match(imageType)) {
     continue;
    }
    var img=document.getElementById("thumbnil");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
   };
   })(img);
  reader.readAsDataURL(file);
  }
  }
</script>
        </div>
    </div>

  <br>
  <center>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <input class="btn btn-primary" type="submit" id="submit" name="submit" value="Submit">&nbsp&nbsp <a  href="{{ route('viewCategorydata') }}"><button type="button" class="btn btn-warning">view data</button></a>
        <br>

</script>
        </center>
</form>
</div>
</body>
</html>
@endsection
