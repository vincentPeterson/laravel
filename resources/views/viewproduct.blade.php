@extends('layout.master')
@section('content')
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script
  src="https://code.jquery.com/jquery-3.5.1.js"
  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
  crossorigin="anonymous"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
function removeProduct(del) {
  let a = del;
  console.log(del);
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this record!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
      .then((willDelete) => {
          if (willDelete) {
              var id = del;
              // console.log(id);
              $.ajax({
                  type: "get",
                  url: "data_delete/" + id,
                  success: function (value) {
                      $("tbody").html(value);
                      location.reload();
                      // return Redirect::back();
                  }
              });
              // swal("Your record has been deleted!", {
              //     icon: "success",
              //     // location.reload();
              // });
          } else {
              swal("Your record is not deleted!", {
                  icon: "error",
              });
          }
      })
}
</script>
{{-- <script type="text/javascript">
$(document).ready(function() {
  $('.servideletebtn').click(function() {
    event.preventDefault();

    swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
})
    .then((willDelete) => {
      var del = $(this).closest('tr').find('.getidforjs').val();

        if (willDelete) {
          $.ajax({
          type: "GET",
           url: 'data_delete/' + del,
          data: {
          "id": del,

          },
         //console.log(data);
        success: function(response) {
          swal(response.status, {
          icon: "success",
          })
          .then((result) => {
          // {{ route('viewCategorydata') }}
          // location.reload();
        // window.location.href = url;
    });
}
});
}
           else {
swal("Your Category is Safe!");
   }
});
});
});
</script> --}}
{{--
<script>
$( document ).ready(function() {

  $('.servideletebtn').click(function() {


  }
    swal({
  title: "Are you sure?",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {

    swal("Poof! Your imaginary file has been deleted!", {
      icon: "success",
    });
  } else {
    swal("Your imaginary file is safe!");
  }
});

});


</script> --}}

<body>
   @if ($message = Session::get('sucess'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
</div>
@endif
<h1> View  product </h1>
<button type="button" class="btn btn-warning"><a href="{{route('Productfrom')}}">Add product</a></button>
<form action="" method="" enctype="multipart/form-data">
  <div class="input-group col-sm-4 float-sm-right">

      {{-- <input class="form-control py-2 border-right-0 border "  type="search"  id="example-search-input"  placeholder="search" name="valueToSearch"> --}}

      <span class="input-group-append">

     {{-- <button class="btn btn-outline-secondary border-left-0 border float-right" type="submit" placeholder="search" name="search" value="Filter">
              <i class="fa fa-search"></i>
          </button> --}}
        </span>
      </div>
    </form>
    <table id="product" class="table table-bordered" style="width:100%">
  <thead>
    <tr>
      <input type="hidden"/>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">category</th>
      <th scope="col">image</th>
      <th scope="col">price</th>
      <th scope="col">saleprice</th>
      <th scope="col">Quentity</th>
      <th scope="col">status</th>
      <th scope="col">create date</th>
      <th scope="col">update date</th>
      <th scope="col">options</th>

    </tr>
  </thead>
  <tbody id="tbody">


</tbody>
</table>
</body>
</form>

@endsection