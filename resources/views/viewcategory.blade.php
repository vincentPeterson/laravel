@extends('masterlayout')
@section('content')
<h2> view  category</h2>
</center>
<a href="{{route('addcategory') }}"><button type="button" class="btn btn-warning">add category</button></a>
<br>
<form action="" method="post" enctype="multipart/form-data">
        <div class="input-group col-sm-4 float-sm-right" >

            <input class="form-control py-2 border-right-0 border "  type="search"  id="example-search-input"  placeholder="search" name="valueToSearch">
            <span class="input-group-append">
            
           <button class="btn btn-outline-secondary border-left-0 border float-right" type="submit" placeholder="search" name="search" value="Filter">
                    <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </div>
   <br> 
<br> 
</br>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">image</th>
      <th scope="col">order</th>
      <th scope="col">status</th>
      <th scope="col">create date</th>
      <th scope="col">update date</th>
      <th scope="col">edit</th>
      <th scope="col">delete</th>
    </tr>
  </thead>
  <tbody>
   
    @foreach($res as $row)
 <tr>
 <td>{{$row->id}}</td>
 <td>{{$row->name}} </td>
 <td><img src="{{asset('public/uploads/'.$row->image)}}" width="50" height="50"></td> 
 <td> {{$row->order}} </td>
 <td>{{$row->status}} </td>
 <td> {{$row->created_at}}</td>
 <td>{{$row->updated_at}}</td>
 
 <td><a href="edit_id/{{$row->id}}"><button type="button" class="btn btn-warning">Edit</button></a></td>
 <td><a href="delete_id/{{$row->id}}"><button type="button" class="btn btn-danger">delete</button></a></td>
     {{-- <td><a onclick = " return myfun()" href="cat_deletebtn.php?delete_id={{$row->id}}"><button type="button" class="btn btn-danger">Delete</button></a></td> --}}
  
</tr>    
  @endforeach
</tbody>
</table>
</form>

@endsection
