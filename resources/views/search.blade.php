@extends('layout.master')
@section('content')
<!DOCTYPE html>
<html>
<head>
{{-- <meta name="_token" content="{{ csrf_token() }}"> --}}
<title>Live Search</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> --}}

</head>
<body>
<div class="container">
<div class="row">
<div class="panel panel-default">
<div class="panel-heading">
<h3>Products info </h3>
</div>
<div class="panel-body">
<div class="form-group">
<input type="text" class="form-controller" id="search" name="search"></input>
</div>
<table class="table table-bordered table-hover">
<thead>
    {{-- <td>'.$product->ProductId.'</td>
    <td>'.$product->vName.'<td>
    <td>'.$product->iCategory.'<td>
    <td>'.$product->fPrice.'<td>
    <td>'.$product->fSalePrice.'<td>
    <td>'.$product->iQuantity.'<td>
    <td>'.$product->Status.'<td>
    <td>'.$product->created_at.'<td>
    <td>'.$product->updated_at.'<td> --}}
    <tr>
        <th scope="col">ProductId</th>
        <th scope="col">name</th>
        <th scope="col">category</th>
        <th scope="col">fPrice</th>
        <th scope="col">saleprice</th>
        <th scope="col">Quantity</th>
        <th scope="col">status</th>
        <th scope="col">created_at</th>
        <th scope="col">updated_at</th>
        <th scope="col">edit</th>
        <th scope="col">delete</th>
      </tr>
</thead>
<tbody >

</tbody>
</table>
</div>
</div>
</div>
</div>

<script type="text/javascript">
$('#search').on('keyup',function(){
$value=$(this).val();
$.ajax({
type : 'get',
url :'{{URL::to('search')}}',
data:{'search':$value},
success:function(data){
$('tbody').html(data);
console.log(data);
}
});
})
</script>
{{-- <script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script> --}}
</body>
</html>
@endsection