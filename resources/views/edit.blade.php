<!DOCTYPE html>
<html lang="en">
<head>
  <title>project</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

  <script type="text/javascript" src="cat_validation.js"></script>
  <style>
#thumbnil{
  height:150px;
  width: 90px;
}
</style>

</head>
<body>
<div>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">myWebSite</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>

      <li><a href="addcategory.php">add category</a></li>
      <li><a href="addproduct.php"> add product</a></li>
      <li><a href="viewcategory.php"> view Category</a></li>
      <li><a href="viewproduct.php"> view product</a></li>
    </ul>
   <button onclick="location.href = 'logout.php';" class="btn btn-danger navbar-btn"  id="logut" style="float:right;">logut</button>
  </div>
</nav>
<div class="container">
    <center>
  <h2>Add category</h2>
  <br>
    <form class="form-horizontal" id="categoryForm" action="{{route('update') }}"  method="POST" enctype="multipart/form-data">
    @csrf
  <div class="form-group">
      <label class="control-label col-sm-2" for="name">name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  id="name"  name="name"  placeholder="Enter name" required="">
    </div>
  </div>

    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="order">order</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" id="order"  name="order"  required="">
      </div>
    </div>
    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="status">status</label>
      <div class="col-sm-10">
        <select class="form-control" name="status" required="">

           <option value="">select status</option>
          <option value="active" name="status" >active</option>
          <option value="inactive" name="status">inactive</option>
        </select>
      </div>
    </div>
    <br>

    <div class="form-group">
        <div class="custom-file">
            <label class="control-label col-sm-2" for="image">image:</label>
            <input type="file" accept="image/*" class="custom-file-left-input" name="image" onchange="showMyImage(this)" id="fileupload" lang="es" required="">
            <br>
            <img id="thumbnil" style=" margin-top:10px;" src="" alt="image"/>
            {{-- <script>
      function showMyImage(fileInput) {
      var files = fileInput.files;
     for (var i = 0; i < files.length; i++) {
     var file = files[i];
     var imageType = /image.*/;
     if (!file.type.match(imageType)) {
     continue;
    }
    var img=document.getElementById("thumbnil");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
   };
   })(img);
  reader.readAsDataURL(file);
  }
  }
</script> --}}
        </div>
    </div>

  <br>
  <center>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <input class="btn btn-primary" type="submit" id="submit" name="submit" value="Submit">&nbsp&nbsp <a  href="viewcategory.php"><button type="button" class="btn btn-warning">view data</button></a>
        <br>

</script>
        </center>
</form>
</div>
</body>
</html>