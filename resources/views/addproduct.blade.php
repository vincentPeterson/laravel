
@extends('masterlayout')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>project</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
 
  <style>
#thumbnil{
  height:150px;
  width: 90px;
}
</style>

</head>
<body>
<div>

<div class="container">

    <center>
  <h2>Add product</h2>
  </center>


  <br>
  <form class="form-horizontal" id="productForm" name="productForm" method="POST" enctype="multipart/form-data" action="">

  <div class="form-group">
      <label class="control-label col-sm-2" for="name">name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="productName" name="name"  placeholder="Enter name" required>
    </div>
  </div>

    <br>
    <div class="form-group">
      <label class="control-label col-sm-2" for="categoryName">category</label>
      <div class="col-sm-10">
        <select class="form-control" name="cname" id="categoryName" required>
            <option>---</option>
            
            @foreach($res as $row)
         
             <option value="{{$row->id}}">
               {{$row->name}}
             </option>

           @endforeach
            
   
        </select>
      </div>
    </div>
    <br>
  <div class="form-group">
      <label class="control-label col-sm-2" for="price">price</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" name="price" id="Price"  placeholder="Enter price" required="">
    </div>
  </div>
  <br>

  <div class="form-group">
      <label class="control-label col-sm-2" for="salePrice">saleprice</label>
      <div class="col-sm-10">
        <input type="number" class="form-control"  id="salePrice" value="" name="salePrice"  placeholder="Enter name" required="">
    </div>
  </div>
  <br>
  <div class="form-group">
      <label class="control-label col-sm-2" for="status">status</label>
      <div class="col-sm-10">
        <select class="form-control" name="status" required="">

           <option value="">select status</option>
          <option value="active" name="status" >active</option>
          <option value="inactive" name="status">inactive</option>
        </select>
      </div>
  </div>
  <br>
  <div class="form-group">
      <label class="control-label col-sm-2" for="quantity">quantity</label>
      <div class="col-sm-10">
        <input type="text" class="form-control"  id="quantity" value="" name="quantity"  placeholder="Enter quantity" required="">
    </div>
  </div>
  <br>
  <div class="form-group">
        <div class="custom-file">
            <label class="control-label col-sm-2" for="image">image:</label>
            <input type="file" class="custom-file-left-input" name="image[]" id="image" lang="es" required=""   multiple>
            <div class="col-sm-offset-2 col-sm-10">
                <input class="btn btn-primary" type="submit" id="submit" name="submit" value="Submit">&nbsp<a href="viewproduct.php"><button type="button" class="btn btn-warning">view data</button></a>
              </div>
            </div>
            </div>
     
  <center>
  </center>
  </form>
</div>
</div>
</body></html>
@endsection