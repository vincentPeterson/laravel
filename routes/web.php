<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('masterlayout');
});
Route::get('/category/list','CategoryController@index')->name('categorylist');
Route::post('/usercontroller','Register@submit');
Route::get('/login', 'Login@index');

Route::post('post-login', 'Login@postLogin')->name('post-login');

Route::get('/cat', 'CategoryController@index')->name('addcategory');
Route::post('/saved','CategoryController@store')->name('saved');
// Route::get('/view','CategoryController@viewform')->name('viewCategory');
Route::get('/viewpage','CategoryController@display')->name('viewCategorydata');

Route::get('/edit_id/{id}','CategoryController@edit');

Route::get('/delete_id/{id}','CategoryController@delete');

Route::put('update/{id}','CategoryController@update')->name('update');

Route::get('/pro', 'ProductController@display')->name('product');
