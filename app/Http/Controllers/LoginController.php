<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Login;
use Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function index()
    {
        return view('loginform');
    }


    public function authenticate(Request $request) {
        $credentials = $request->only('email', 'password');
        // dd($credentials);
        $auth = Auth::guard('admin');
        if ($auth->attempt($credentials)) {
            //  return 'success';

            return redirect()->route('viewCategorydata');
        }
        // return 'Error';

        // return view('login');
           return redirect()->route('login')->with('alert-info Login-password', 'Login Fail, please check email or password');
     }

     public function logout() {
        Auth()->guard('admin')->logout();
        return view('loginform');
    }

}

