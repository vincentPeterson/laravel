<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\category;


class CategoryController extends Controller
{
    public function index(){
    return view('category.addcategory');
    }

    public function store(Request $request){
        $addcat= new category();
        $addcat->name=$request->input('name');
        $addcat->order=$request->input('order');
        $addcat->status=$request->input('status');
        // $add_cat->image->$req->input('image');
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move(public_path('uploads'),$filename);
            $addcat->image = $filename;
          }else{
            return $request;
            $addcat->image  = '';
          }
        $addcat->save();
        return redirect('viewpage');
    }

// public function insert(Request $Request){
//     // return view('category.addcategory');
//     $Request->file('image')->store('public');
//     return "image Uploaded...";
//     }

public function viewform(){
    return view('viewcategory');

}

public function display(){
  $res=category::all();
  return view('viewcategory')->with('res', $res);
  }
  public function edit($id){ 
  $edit =category::find($id);
  return view('category.addcategory')->with('edit',$edit);
  }

  public function update(Request $request,$id){
    $addcat = category::find($id);
  
    // $addcat= new category();
    
    // $add_cat->image->$req->input('image');
    if($request->hasfile('image')){
         $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time().'.'.$extension;
        $file->move(public_path('uploads'),$filename);
        $addcat->image = $filename;
      
    // $addcat->save();
    }
    $addcat->name=$request->input('name');
    $addcat->order=$request->input('order');
    $addcat->status=$request->input('status');
    $addcat->save();

    return redirect('viewpage');
    //  return redirect('viewpage')->with('addcat',$addcat);
}

    
    public function delete($id){
      // print_r( $id);
      
      // $add=category::find($id);
      // $add->delete();
      category::destroy(array('id',$id));
      return redirect('viewpage');
      // return view('addcategory')->with('add', $add);
        }
  }
