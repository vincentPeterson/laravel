<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function index()
    {
        return view('search');
    }
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $products = DB::table('product')->where('product.vName', 'LIKE', '%' . $request->search . "%")->get();

        } else {
            $products = DB::table('product')
                ->orderBy('id', 'desc')
                ->get();
        }
        // dd($products);
        // // die();
        // if ($products) {
            foreach ($products as $key => $product) {
                $output .= '<tr>' .
                '<td>' . $product->ProductId . '</td>' .
                '<td>' . $product->vName . '</td>' .
                '<td>' . $product->iCategory . '</td>' .
                '<td>' . $product->fPrice . '</td>' .
                '<td>' . $product->fSalePrice . '</td>' .
                '<td>' . $product->iQuantity . '</td>' .
                '<td>' . $product->Status . '</td>' .
                '<td>' . $product->created_at . '</td>' .
                '<td>' . $product->updated_at . '</td>' .
                    '</tr>';
            }
            return Response($output);
        }
    }

