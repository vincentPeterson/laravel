<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Login extends Authenticatable
{

    protected $table = 'user';
    public $timestamps = false;

    protected $fillable = [
        'email',
        'password',
    ];

}
