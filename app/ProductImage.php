<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{


    protected $table = 'image';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable =['vImage','eStatus','ProductId'];

    public function productImage() {
        return $this->belongsToMany(ProductImage::class);
    }

    public function getProductImage() {
        return $this->productImage->pluck('vImage');
  }
}

